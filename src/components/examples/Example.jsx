import React, { Component } from 'react'
import axios from 'axios';

class Example extends Component {

    constructor(props) {
        super();
    }

    state = {
        title: "My Example",
        numbers: [1, 2, 3]
    }

    onChangeMyTitleInBadWay = (newTitle) => {
        //will throw an error at runtime
        this.state.title = newTitle;
    }

    onChangeMyTitle = (newTitle) => {
        /* WRONG!
         this.setState({...this.state, title: newTitle});
         console.log('Updated State', this.state.title);
         */

        this.setState({...this.state, title: newTitle})
        .then(udpatedState => {
            console.log('Updated State', udpatedState);
        });
    }

    componentDidMount = () => {
        axios.get(this.props.titleUrl).then(titleObj => {
            this.setState({...this.state, title: titleObj });
        }).catch(reason => {
            console.error(reason);
            this.setState({...this.state, title: "" });
        });
    }

    handleChangeMyTitle = (newTitle) => {
        this.setState({...this.state, title: newTitle })
            .then(updatedState => {
                axios.put(this.props.titleUrl).catch(reason => {
                    console.error(reason);
                })
            });
    }

    handleUserEmailPrefs = (newEmail, emailType) => {
        const { userUrl, prefsUrl } = this.props;

        axios.all(
            axios.put(userUrl, newEmail),
            axios.put(prefsUrl, emailType)
        )
            .then(axios.spread((user, prefs) => {
                this.setState({...this.state,
                     user: user, prefs: prefs
                });
            }));
    }

    addFour = () => {
        const newNumbers = this.state.numbers.slice();
        newNumbers.push(4);
        this.setState({ ...this.state, numbers: newNumbers });
    }

    render() {
        return (
            <div>
                <h1 className="title">{this.state.title}</h1>
                <button type="button"
                    className="btn btn-outline-danger"
                    onClick={this.addFour}
                >Add Four</button>
            </div>
        );
    }
}

export default Example;

/*
class Example2 extends Component {

    state = {
        title = "My Example"
    }

    render() {
        return (
            React.createElement(
                'div',
                'h1',
                { className: 'Title' },
                this.state.title
            )
        );
    }
}
*/

/*

class AxiosExample extends Component {
    state = {}

    componentDidMount = () => {

        axios.get().then().catch();

        axios.put().then().catch();

        axios.post().then().catch();

        axios.delete().then().catch();

        axios.head().then().catch();

        axios.all([axios.get(), axios.get()])
            .then(axios.spread());


    }

    render() {
        return (<div></div>);
    }
}

export default AxiosExample;


<div>
    <h1 className="title">{this.state.title}</h1>
</div>

React.createElement(
    'div',
    'h1',
    { className: 'title' },
    this.state.title
)


<div>
    <h1 class='title'>My Example</h1>
</div>
*/



