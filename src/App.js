import React, { Component } from 'react';
import logo from './logo.svg';
import './App.scss';
import Navbar from './components/Navbar';
import Notes from './components/Notes';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Navbar></Navbar>
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <Notes></Notes>
        </header>
      </div>
    );
  }
}

export default App;
